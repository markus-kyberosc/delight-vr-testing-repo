async function getCameraSelection() {
  const cameraOptions = document.getElementById("video-options");
  const devices = await navigator.mediaDevices.enumerateDevices();
  const videoDevices = devices.filter(device => device.kind === 'videoinput');
  const options = videoDevices.map(videoDevice => {
    return `<option value="${videoDevice.deviceId}">${videoDevice.label}</option>`;
  });
  cameraOptions.innerHTML = options.join('');
};

function publish() {
  const publisher = new window.red5prosdk.RTCPublisher();
  const cameraDeviceId = document.getElementById("video-options").value;

  window.red5ProPublisher = publisher;

  return publisher
    .init(window.getPublisherConfig(cameraDeviceId))
    .then(() => publisher.publish())
    .catch((error) => console.error(error));
}

function unPublish() {
  window.red5ProPublisher.unpublish().then(() => {
    const stream = window.red5ProPublisher.getMediaStream();
    const tracks = stream.getTracks();
    tracks.forEach((track) => track.stop());
  });
}

function subscribe() {
  createPlayer();

  const red5prosdk = window.red5prosdk;

  const subscriber = new red5prosdk.RTCSubscriber();

  subscriber.on(red5prosdk.SubscriberEventTypes.PLAY_UNPUBLISH, () => {
    subscriber.off("*", () => {});
    subscriber.unsubscribe();
  });

  subscriber
    .init(window.subscriberConfig)
    .then((subscriber) => subscriber.subscribe())
    .then((subscriber) => {
      const mediaStream = subscriber.getMediaStream();
      console.log({ subscriber, mediaStream });
      if (mediaStream) {
        updateMediaStream(mediaStream);
      }
    })
    .catch((error) => console.error(error));
}

function createPlayer() {
  const container = document.getElementById("player-container");
  const player = document.createElement("dl8-live-video");
  const source = document.createElement("source");

  player.setAttribute("format", "MONO_FLAT"); // change MONO_FLAT to MONO_360 to reproduce the bug.
  player.setAttribute('not-pausable', 'true');

  player.appendChild(source);

  window.livePlayer = player;

  container.appendChild(player);
}

function updateMediaStream(mediaStream) {

  if (mediaStream && window.livePlayer) {

    const observer = new MutationObserver(() => {
      // We need waiting for the video tag is ready
      setTimeout(() => {
        const video = window.livePlayer.querySelector("video");
        if (video && !video.srcObject) {
          video.srcObject = mediaStream;
          video.play();
          observer.disconnect();
        }
      });
    });
    observer.observe(window.livePlayer, { childList: true, subtree: true });
  }
}

(() => {
  getCameraSelection()
  .then(() => {
    document.getElementById("start").addEventListener("click", () => {
      publish().then(subscribe);
    });
    document.getElementById("stop").addEventListener("click", unPublish);
  })
  .catch((error) => console.log(error));

})();
