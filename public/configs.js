(() => {
  window.getPublisherConfig = function (videoDeviceId) {
    return {
      protocol: "wss",
      port: 443,
      host: "test-media-stream.syminar.com",
      app: "live",
      streamName: "syminar-test-delight-vr",
      rtcConfiguration: {
        iceServers: [{ urls: "stun:stun2.l.google.com:19302" }],
        iceCandidatePoolSize: 2,
        bundlePolicy: "max-bundle"
      },
      streamMode: "live",
      mediaElementId: "red5pro-publisher",
      bandwidth: {
        audio: 56,
        video: 512
      },
      mediaConstraints: {
        audio: true,
        video: {
          width: {
            exact: 640
          },
          height: {
            exact: 480
          },
          frameRate: {
            min: 8,
            max: 24
          }
        }
      },
      onGetUserMedia: function () {
        const videoConstraint = {
          width: {
            max: 1920,
            ideal: 1280,
            min: 640
          },
          width: {
            max: 1080,
            ideal: 720,
            min: 360
          },
        };

        if (videoDeviceId) {
          videoConstraint.deviceId = {
            exact: videoDeviceId
          }
        }
        return navigator.mediaDevices.getUserMedia({
          audio: false,
          video: videoConstraint,
        })
      }
    }
  };
  
  window.subscriberConfig = {
    protocol: "wss",
    port: 443,
    host: "test-media-stream.syminar.com",
    app: "live",
    streamName: "syminar-test-delight-vr",
    rtcConfiguration: {
      iceServers: [{ urls: "stun:stun2.l.google.com:19302" }],
      iceCandidatePoolSize: 2,
      bundlePolicy: "max-bundle"
    },
    subscriptionId: "mystream" + Math.floor(Math.random() * 0x10000).toString(16),
    videoEncoding: "NONE",
    audioEncoding: "NONE",
    mediaElementId: "red5pro-subscriber"
  };
})();
