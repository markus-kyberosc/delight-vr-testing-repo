# This is repository that for Delight VR testing.

This repo is use for reproduce the bug while play a live stream 360-dgree video with `dl8-live-video` module.

Bug detail:

> When we live with a 360-degree video, the `dl8-live-video` player cannot be
playable and show a message `"Content issue: no playable video source found"`.

> I investigated this issue, the reason is the `dl8-live-video` module does not
receive the source (we have to provide a blank `source` tag for it before)
when receiving video format as `MONO_360` >> `dl8-live-video` module does not
generate a `video` tag. So we couldn't provide `MediaStream` for it to work.

> To reproduce this bug please change the `format` attribute of `dl8-live-video` to `MONO_360` in file `./public/main.js` (function `createPlayer()`).

> I tried to create a `video` tag on my own and assign a `MediaStream` that received from Red5Pro, it still works normally.

## Requirement:

* Install [Node.js](https://nodejs.org/) to run.

## Installation:

```sh
$ cd ./delight-vr-testing-repo
$ npm install
$ npm run start
```

Verify the example running by navigating to [localhost:3000](localhost:3000) in your preferred browser.

```sh
localhost:3000
```
